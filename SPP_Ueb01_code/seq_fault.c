#include <stdlib.h>
#include <stdio.h>

typedef struct 
{
  char* name;
  int matriculation_number;
  int semester; 
  char* program;
} student;

student* create_student( char* name,
		int matriculation_number,
		int semester,
		char* program )
{
  student new_student;
  student* new_student_ptr = &new_student;
  
  new_student_ptr->name    = name;
  new_student_ptr->matriculation_number    = matriculation_number;
  new_student_ptr->semester = semester;
  new_student_ptr->program  = program;
  return new_student_ptr;
}

int main()
{
  student* s1 = create_student( "Max Mustermann", 424242, 1, "Computer Science" );
  //printf(s1->name);
  free( s1 );
  return 0;
}

