#include <stdlib.h>
#include <stdio.h>
#include "sorted_list.h"

struct SortedLinkedListNode{
	int value;
	SortedLinkedListNode* next;
};

struct SortedLinkedList{
	SortedLinkedListNode* head;
};


SortedLinkedList* SortedLinkedList_create(){
	SortedLinkedList* list_ptr= malloc(sizeof(SortedLinkedList));
	list_ptr->head=NULL;
	return list_ptr;
}

void SortedLinkedList_addToList(SortedLinkedList* list, int data ){
	// erstelle Node mit entsprechendem Inhalt
	SortedLinkedListNode* node = malloc(sizeof(SortedLinkedListNode));
	node->value = data;	
	node->next = NULL;
	
	// --- einfügen ---
	//leere Liste
	if (list->head == NULL){
		list->head = node;
		return;
	}
	// in head einfügen
	else if(list->head->value >= data){
		node->next = list->head;
		list->head = node;
		return;
	}
	// irgendwo anders einfügen
	SortedLinkedListNode* prev = list->head;
	SortedLinkedListNode* insertAtNode = list->head->next;
	while(1){
		// am Ende einfügen
		if (insertAtNode == NULL){
			prev->next = node;
			return;
		}
		// beim nächsten einfügen
		if (insertAtNode->value < data){ //bsp 8 einfügen bei 2
			prev = insertAtNode;
			insertAtNode = insertAtNode->next;
			continue;
		}
		// hier einfügen
		else{
			node->next = insertAtNode;
			prev-> next = node;
			return;
		}
	}
}


void SortedLinkedList_delete(SortedLinkedList* list ){
	if (list==NULL){
		return;
	}
	SortedLinkedListNode* current = list->head;
	SortedLinkedListNode* next;
	int i = 1;
   while (current != NULL)  
   { 
		printf("delete element: %d \n", i); 
		i++;
       next = current->next; 
       free(current); 
       current = next; 
   } 
   free(list);
}

SortedLinkedListNode* SortedLinkedList_getSmallest(SortedLinkedList* list){
	if (list != NULL) {
		return list->head;
	}
	return NULL;

}

void main(){
	SortedLinkedList* list=SortedLinkedList_create();
	SortedLinkedList_addToList(list, 8);
	printf("add node 8 \n");
	SortedLinkedList_addToList(list, 2);
	printf("add node 2 \n");
	SortedLinkedList_addToList(list, 5);
	printf("add node 5 \n");
	SortedLinkedList_addToList(list, 9);
	printf("add node 9 \n");
	SortedLinkedList_addToList(list, 8);
	printf("add node 8 \n");
	
	if (list->head != NULL)
		printf("head: %d \n", list->head->value);
	SortedLinkedListNode* node = list->head->next;
	while (node != NULL){
		printf("next: %d \n", node->value);
		node = node->next;
	}
	
	int small = SortedLinkedList_getSmallest(list)->value;
	printf("Smallest: %d \n", small);
	
	SortedLinkedList_delete(list);
}