#ifndef SORTED_LIST_H
#define SORTED_LIST_H

typedef struct SortedLinkedListNode SortedLinkedListNode;
typedef struct SortedLinkedList SortedLinkedList;

SortedLinkedList* SortedLinkedList_create();

void SortedLinkedList_addToList( SortedLinkedList* list, int data );

void SortedLinkedList_delete( SortedLinkedList* list );

SortedLinkedListNode* SortedLinkedList_getSmallest( SortedLinkedList* list );

#endif
