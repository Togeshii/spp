#include <cmath>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include "mpi.h"
#include <string>

using namespace std;

int main(int argc, char *argv[])

//
//  Purpose:
//
//    MAIN is the main program for HEATED_PLATE_MPI.
//
//  Discussion:
//
//    This code solves the steady state heat equation on a rectangular region.
//
//    The sequential version of this program needs approximately
//    18/epsilon iterations to complete.
//
//
//    The physical region, and the boundary conditions, are suggested
//    by this diagram;
//
//                   W = 0
//             +------------------+
//             |                  |
//    W = 100  |                  | W = 100
//             |                  |
//             +------------------+
//                   W = 100
//
//    The region is covered with a grid of M by N nodes, and an M by N
//    array W is used to record the temperature.  The correspondence between
//    array indices and locations in the region is suggested by giving the
//    indices of the four corners:
//
//                  I = 0
//          [0][0]-------------[0][N-1]
//             |                  |
//      J = 0  |                  |  J = N-1
//             |                  |
//        [M-1][0]-----------[M-1][N-1]
//                  I = M-1
//
//    The steady state solution to the discrete heat equation satisfies the
//    following condition at an interior grid point:
//
//      W[Central] = (1/4) * ( W[North] + W[South] + W[East] + W[West] )
//
//    where "Central" is the index of the grid point, "North" is the index
//    of its immediate neighbor to the "north", and so on.
//
//    Given an approximate solution of the steady state heat equation, a
//    "better" solution is given by replacing each interior point by the
//    average of its 4 neighbors - in other words, by using the condition
//    as an ASSIGNMENT statement:
//
//      W[Central]  <=  (1/4) * ( W[North] + W[South] + W[East] + W[West] )
//
//    If this process is repeated often enough, the difference between
//    successive estimates of the solution will go to zero.
//
//    This program carries out such an iteration, using a tolerance specified by
//    the user.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    18 October 2011
//
//  Author:
//
//    Original C version by Michael Quinn.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Michael Quinn,
//    Parallel Programming in C with MPI and OpenMP,
//    McGraw-Hill, 2004,
//    ISBN13: 978-0071232654,
//    LC: QA76.73.C15.Q55.
//
//  Local parameters:
//
//    Local, double DIFF, the norm of the change in the solution from one
//    iteration to the next.
//
//    Local, double MEAN, the average of the boundary values, used to initialize
//    the values of the solution in the interior.
//
//    Local, double U[M][N], the solution at the previous iteration.
//
//    Local, double W[M][N], the solution computed at the latest iteration.
//
{
#define M 500
#define N 500

  double epsilon = 0.001;
  int i;
  int iterations;
  int iterations_print;
  int j;
  double mean;
  double wtime;
  int done = 0;
  
  // initialize MPI
  int myid, num_procs;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  int num_worker = num_procs-1;
  int master = 0;
  MPI_Status status;
  int w_len;
  int req_len;
  double diff; 
  

  if (myid == master){ // master
  
	  // it is sufficient if only one process does the output
	  cout << "\n";
	  cout << "HEATED_PLATE_OPENMP\n";
	  cout << "  C++/MPI version\n";
	  cout << "  A program to solve for the steady state temperature distribution\n";
	  cout << "  over a rectangular plate.\n";
	  cout << "\n";
	  cout << "  Spatial grid of " << M << " by " << N << " points.\n";
	  cout << "  The iteration will be repeated until the change is <= " << epsilon
		   << "\n";
	  cout << "  Number of processes available = " << num_procs << "\n";
	  
	  //  Average the boundary values, to come up with a reasonable
	  //  initial value for the interior.
	  mean = 100*(N+2*M-4) / (double)(2 * M + 2 * N - 4);
	  cout << "\n";
	  cout << "  MEAN = " << mean << "\n";
	  MPI_Bcast(&mean, 1, MPI_DOUBLE, master, MPI_COMM_WORLD); // TODO Ist das nötig oder weiß eh jeder was mean ist, weil es oben initialisiert wurde?
	  
	  // each worker gets at least one line
	  if (M < num_worker){
		  MPI_Finalize(); //TODO Ist das Programm so richtig abgebrochen oder nur der eine Prozess?
		  return 0;
	  }
	  int rows_procs = floor(M/num_worker);
	  for(i = 1; i<num_procs; i++){
		  if(i == num_worker-(M%num_worker)+1)
			  rows_procs++;
		  MPI_Send(&rows_procs, 1, MPI_INT, i, master, MPI_COMM_WORLD);
	  }
	  
	  //
	  //  iterate until the  new solution W differs from the old solution U
	  //  by no more than EPSILON.
	  //
	  iterations = 0;
	  iterations_print = 1;
	  cout << "\n";
	  cout << " Iteration  Change\n";
	  cout << "\n";
	  wtime = MPI_Wtime();
	  diff = epsilon;

	  // this statement should yield the same result on all processes, so that all
	  // processes stop at the same iteration
	  while (epsilon <= diff) {
		  
		MPI_Bcast(&done, 1, MPI_INT, master, MPI_COMM_WORLD);

		diff = 0.0;
		MPI_Reduce(&diff, &diff, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

		iterations++;
		// it is sufficient if only one process does the output
		if (iterations == iterations_print) {
		  cout << "  " << setw(8) << iterations << "  " << diff << "\n";
		  iterations_print = 2 * iterations_print;
		}
	  }
	  done = 1;
	  MPI_Bcast(&done, 1, MPI_INT, master, MPI_COMM_WORLD);
	  // please insert a Barrier before time measurement
	  MPI_Barrier(MPI_COMM_WORLD);
	  wtime = MPI_Wtime() - wtime;

	  // it is sufficient if only one process does the output
	  cout << "\n";
	  cout << "  " << setw(8) << iterations << "  " << diff << "\n";
	  cout << "\n";
	  cout << "  Error tolerance achieved.\n";
	  cout << "  Wallclock time = " << wtime << "\n";
	  //
	  //  Terminate.
	  //
	  cout << "\n";
	  cout << "HEATED_PLATE_OPENMP:\n";
	  cout << "  Normal end of execution.\n";

  } else { // worker
	MPI_Bcast(&mean, 1, MPI_DOUBLE, master, MPI_COMM_WORLD);
	int my_rows_procs;
	MPI_Recv(&my_rows_procs, 1, MPI_INT, master, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	
	if(myid == 1 || myid == num_worker){
		w_len = my_rows_procs+1; // meine Zeilen + 1 vom nächsten Prozess als Zwischenspeicher
		req_len = 2;
	}else{
		w_len = my_rows_procs+2;
		req_len = 4;
	}
	double w[w_len][N];
	double u[w_len][N];
	MPI_Request request[req_len];
	
	// Initialization
	if(myid == 1){ // oberer Rand
		for(j=0; j<N; j++){ // erste Zeile
			w[0][j] = 0.0;
		}
		for(i=1; i<w_len; i++){ // restliche Zeilen
			w[i][0] = 100.0;
			w[i][N-1] = 100.0;
			for(j=1; j<N-1; j++){
				w[i][j] = mean;
			}
		}
	}
	if(myid == num_worker){ // unterer Rand
		for(i=0; i<w_len-1; i++){
			w[i][0] = 100.0;
			w[i][N-1] = 100.0;
			for(j=1; j<N-1; j++){
				w[i][j] = mean;
			}
		}
		for(j=0; j<N; j++){ // letzte Zeile
			w[w_len-1][j] = 100.0;
		}
	}
	if(myid != 1 || i != num_worker){ // Mitte
		for(i=0; i<w_len; i++){
			w[i][0] = 100.0;
			w[i][N-1] = 100.0;
			for(j=1; j<N-1; j++){
				w[i][j] = mean;
			}
		}
	}
	
	MPI_Bcast(&done, 1, MPI_INT, master, MPI_COMM_WORLD);
	
	while(!done){		
		// TODO gerne nochmal drüber schauen
		
		// swap current rows (non-blocking version)
		// tag 1, if senders upper row -- tag 2, if senders lower row
		if(myid != 1){ // obere Zeile senden und empfangen
			MPI_Isend(&w[1][0], N, MPI_DOUBLE, myid-1, 1, MPI_COMM_WORLD, &request[0]);
			MPI_Irecv(&w[0][0], N, MPI_DOUBLE, myid-1, 2, MPI_COMM_WORLD, &request[1]);
		}
		if(myid != num_worker){ // untere Zeile senden und empfangen
			if(myid == 1){
				MPI_Isend(&w[w_len-1][0], N, MPI_DOUBLE, myid+1, 2, MPI_COMM_WORLD, &request[0]);
				MPI_Irecv(&w[w_len-2][0], N, MPI_DOUBLE, myid+1, 1, MPI_COMM_WORLD, &request[1]);
			}else{
				MPI_Isend(&w[w_len-1][0], N, MPI_DOUBLE, myid+1, 2, MPI_COMM_WORLD, &request[2]);
				MPI_Irecv(&w[w_len-2][0], N, MPI_DOUBLE, myid+1, 1, MPI_COMM_WORLD, &request[3]);
			}
		}
		if(myid == 1 || myid == num_worker)
			MPI_Waitall(2, request, MPI_STATUSES_IGNORE);
		else
			MPI_Waitall(4, request, MPI_STATUSES_IGNORE);
		
		//
		//  Save the old solution in U.
		//
		for (i = 0; i < w_len; i++) {
		  for (j = 0; j < N; j++) {
			u[i][j] = w[i][j];
		  }
		}
		
		//
		//  Determine the new estimate of the solution at the interior points.
		//  The new solution W is the average of north, south, east and west
		//  neighbors.
		// 	outer cells are not to overwrite, so each worker ignores the upper and lower row 
		for (i = 1; i < w_len - 1; i++) {
		  for (j = 1; j < N - 1; j++) {
			w[i][j] = (u[i - 1][j] + u[i + 1][j] + u[i][j - 1] + u[i][j + 1]) / 4.0;
		  }
		}
		
		// diff
		diff = 0.0;
		for (i = 1; i < w_len - 1; i++) {
		  for (j = 1; j < N - 1; j++) {
			if (diff < fabs(w[i][j] - u[i][j])) {
			  diff = fabs(w[i][j] - u[i][j]);
			}
		  }
		}
		MPI_Reduce(&diff, &diff, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
		MPI_Bcast(&done, 1, MPI_INT, master, MPI_COMM_WORLD);
	}
	// Barrier before time measurement
	MPI_Barrier(MPI_COMM_WORLD);
  }
  
  MPI_Finalize();
  return 0;

#undef M
#undef N
}
