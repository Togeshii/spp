#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <assert.h>

#include "mpi.h"

#define STATESIZE 64

// returns random float in range -1 to 1
double rand_dbl(struct random_data *random_buf) {
  int rand_int;
  random_r(random_buf, &rand_int);
  return (double)rand_int / RAND_MAX * 2.0 - 1.0;
}

int main(int argc, char *argv[]) {
  // reading the number of points to use from the argument
  unsigned long num_points = 100000;
  if (argc > 1) {
    num_points = atol(argv[1]);
  }
  
  int myid, num_procs;
  int master = 0;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);
  int num_worker = num_procs - 1;
  MPI_Status status;
  
  if((unsigned)num_worker > num_points){
	  num_points = num_worker;
  }
  
  if(myid == master){ // master code
  
	  int inCircle = 0;
	  int inCircle_procs = 0;
	  int i;
	  int procs_bonus = num_points%num_worker;
	  
	  // number of points each process has to generate
	  int num_points_procs = floor(num_points/num_worker);
	  printf("Schon vor dem Zuweisen gestorben");
	  for (i = 1; i <= num_worker; i++){ // send to each worker
		  // last (num_points%num_worker) workers should generate one more point
		  if(procs_bonus>0) {
		      printf("procs_bonus: %i      Schleifennummer: %i\n", procs_bonus, i);
              num_points_procs++;
              procs_bonus--;
          }
		  
		  MPI_Send(&num_points_procs, 1, MPI_INT, i, master, MPI_COMM_WORLD);
	  }
      printf("Jetzt komme ich zum empfangen");

	  for(i=0; i<num_worker; i++){ // recieve from each worker
		  MPI_Recv(&inCircle_procs, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		  inCircle = inCircle + inCircle_procs;
	  }
	  
	  double pi = 4 * (double)inCircle / num_points;
	  printf("Pi is approx. %f\n", pi);
	  
  }else{ // worker code
	  	  
	  // get number of points to be generated
	  int my_num_points = 0;
	  MPI_Recv(&my_num_points, 1, MPI_INT, master, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	  
	  // own random state
	  struct random_data *random_buf = calloc(1, sizeof(struct random_data));
	  char *state_buf = calloc(STATESIZE, sizeof(char));
  	  assert(random_buf != NULL && state_buf != NULL);
	  unsigned int seed = myid; // processes should use a different random seed
	  initstate_r(seed, state_buf, STATESIZE, random_buf);
		  
	  int my_inCircle = 0;
	  double x,y;
	  for(int i=1; i<=my_num_points; i++){
		  x = rand_dbl(random_buf);
		  y = rand_dbl(random_buf);
		  if(pow(x,2)+pow(y,2) <= 1) // im Kreis
			  my_inCircle++;
		}
	  MPI_Send(&my_inCircle, 1, MPI_INT, master, myid, MPI_COMM_WORLD);

      free(random_buf);
      free(state_buf);
	}
  
  MPI_Finalize();
  return 0;
}
