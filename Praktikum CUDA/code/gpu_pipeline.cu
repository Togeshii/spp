//
//  gpu_pipeline.cu
//
//  Created by Arya Mazaheri on 01/12/2018.
//
//  modified by Yannic Fischler on 29/01/2020
//

#include "gpu_pipeline.h"

//#include <cuda.h>
//#include <cuda_runtime.h>
//#include <device_launch_parameters.h>

#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

/*********** Gray Scale Filter  *********/
// Converts a given 24bpp image into 8bpp grayscale using the GPU.
__global__
void cuda_grayscale(int width, int height, BYTE *image, BYTE *image_out) {
  //Aufgabe 4 (9 pts): implement grayscale filter kernel
  int thread_x = blockIdx.x * blockDim.x + threadIdx.x;
  int thread_y = blockIdx.y * blockDim.y + threadIdx.y;
  int thread = thread_x + thread_y * width;
  
	if (thread > width*height)
		return;

	int pixel_out = thread;
	int pixel_in = 3 * thread;
	
    BYTE *pixel = &image[pixel_in];
    image_out[pixel_out] =
		pixel[0] * 0.2126f + // R
		pixel[1] * 0.7152f + // G
		pixel[2] * 0.0722f ; // B
}

// 1D Gaussian kernel array values of a fixed size (make sure the number > filter size d)
//Aufgabe 7: Define the cGaussian array on the constant memory (2 pts)
__constant__ float cGaussian[64];
//__host__
void cuda_updateGaussian(int filter_radius, double sd) {
  float fGaussian[64];
  for (int i = 0; i < 2*filter_radius+1; ++i) {
    float x = i - filter_radius;
    fGaussian[i] = expf(-(x*x) / (2*sd*sd));
  }
  //Aufgabe 7: Copy computed fGaussian to the cGaussian on device memory (2 pts)
  cudaMemcpyToSymbol(cGaussian, &fGaussian, 64*sizeof(float), 0, cudaMemcpyHostToDevice); // (symbol, *src, count, offset, kind)
}

//Aufgabe 8: implement cuda_gaussian() kernel (3 pts)
inline __host__ __device__ double cuda_gaussian(float x, double sigma) {
	return expf(-(powf(x, 2)) / (2 * powf(sigma, 2)));
}



/*********** Bilateral Filter  *********/
// Parallel (GPU) Bilateral filter kernel
__global__
void cuda_bilateral_filter(BYTE* input, BYTE* output, int width, int height, int filter_radius, double sI, double sS) {
  //Aufgabe 9 (9pts): implement bilateral filter kernel
  int thread_x = blockIdx.x * blockDim.x + threadIdx.x;
  int thread_y = blockIdx.y * blockDim.y + threadIdx.y;
  int thread = thread_x + thread_y * width;
  
	if (thread > width*height)
		return;

	int h = thread / width;
	int w = thread % width;
	
	double iFiltered = 0;
	double wP = 0;
	// Get the centre pixel value
	unsigned char centrePx = input[thread];
	// Iterate through filter size from centre pixel
	for (int dy = -filter_radius; dy <= filter_radius; dy++) {
		int neighborY = h+dy;
		if (neighborY < 0)
			neighborY = 0;
		else if (neighborY >= height)
			neighborY = height - 1;
		for (int dx = -filter_radius; dx <= filter_radius; dx++) {
			int neighborX =w+dx;
			if (neighborX < 0)
				neighborX = 0;
			else if (neighborX >= width)
				neighborX = width - 1;
			// Get the current pixel; value
			unsigned char currPx = input[neighborY*width+neighborX];
			// Weight = 1D Gaussian(x_axis) * 1D Gaussian(y_axis) * Gaussian(Range or Intensity difference)
			double w = (cGaussian[dy + filter_radius] * cGaussian[dx + filter_radius]) * cuda_gaussian(centrePx - currPx, sI);
			iFiltered += w * currPx;
			wP += w;				
		}
	}
	output[thread] = iFiltered / wP;
}

void gpu_pipeline(const Image & input, Image & output, int filter_radius, double sI, double sS) {
  // Events to calculate gpu run time
  cudaEvent_t start, stop;
  float time;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  // GPU related variables
  BYTE *d_input = NULL;
  BYTE *d_image_out[2] = {0}; //temporary output buffers on gpu device
  int image_size = input.cols*input.rows;
  int suggested_blockSize;   // The launch configurator returned block size 
  int suggested_minGridSize; // The minimum grid size needed to achieve the maximum occupancy for a full device launch

  // ******* Grayscale kernel launch *************

  //Creating the block size for grayscaling kernel
  cudaOccupancyMaxPotentialBlockSize( &suggested_minGridSize, &suggested_blockSize, cuda_grayscale);

  int block_dim_x, block_dim_y;
  block_dim_x = block_dim_y = (int) sqrt(suggested_blockSize); 

  //Aufgabe 1: 2pts 
  dim3 gray_block(block_dim_x, block_dim_y);

  //Aufgabe 1: Calculate grid size to cover the whole image (2 pts)
  int grid_dim_x, grid_dim_y;
  grid_dim_x = (int) ceil((double)input.cols/block_dim_x);
  grid_dim_y = (int) ceil((double)input.rows/block_dim_y);
  dim3 gray_grid(grid_dim_x, grid_dim_y);

  // Allocate the intermediate image buffers for each step
  Image img_out(input.cols, input.rows, 1, "P5");
  BYTE *dev[2];
  for (int i = 0; i < 2; ++i) {  
    //Aufgabe 2: allocate memory on the device (3 pts)
	cudaMalloc((void**)&dev[i], sizeof(BYTE)*image_size);
    //Aufgabe 2: intialize allocated memory on device to zero (3 pts)
	cudaMemset((void*)dev[i], 0, sizeof(BYTE)*image_size);
  }

  //copy input image to device
  //Aufgabe 3: Allocate memory on device for input image (2 pts)
  BYTE *inputImage;
  cudaMalloc((void**)&inputImage, sizeof(BYTE)*image_size*3);
  //Aufgabe 3: Copy input image into the device memory (2 pts)
  cudaMemcpy(inputImage, input.pixels, sizeof(BYTE)*image_size*3, cudaMemcpyHostToDevice);

  cudaEventRecord(start, 0); // start timer
  // Convert input image to grayscale
  //Aufgabe 4: Launch cuda_grayscale() (2 pts)
  cuda_grayscale<<<gray_grid, gray_block>>>(input.cols, input.rows, inputImage, dev[0]); // (int width, int height, BYTE *image, BYTE *image_out)
  cudaEventRecord(stop, 0); // stop timer
  cudaEventSynchronize(stop);

  // Calculate and print kernel run time
  cudaEventElapsedTime(&time, start, stop);
  cout << "GPU Grayscaling time: " << time << " (ms)\n";
  cout << "Launched blocks of size " << gray_block.x * gray_block.y << endl;

  //Aufgabe 5: transfer image from device to the main memory for saving onto the disk (2 pts)
  cudaMemcpy(img_out.pixels, dev[0], sizeof(BYTE)*image_size, cudaMemcpyDeviceToHost);
  savePPM(img_out, "image_gpu_gray.ppm");


  // ******* Bilateral filter kernel launch *************

  //Creating the block size for grayscaling kernel
  cudaOccupancyMaxPotentialBlockSize( &suggested_minGridSize, &suggested_blockSize, cuda_bilateral_filter); 

  block_dim_x = block_dim_y = (int) sqrt(suggested_blockSize); 

  //Aufgabe 6: (1 pt)
  dim3 bilateral_block(block_dim_x, block_dim_y);

  //Aufgabe 6: Calculate grid size to cover the whole image (1 pt)
  grid_dim_x = (int) ceil((double)input.cols/block_dim_x);
  grid_dim_y = (int) ceil((double)input.rows/block_dim_y);
  dim3 bilateral_grid(grid_dim_x, grid_dim_y);

  // Create gaussain 1d array
  cuda_updateGaussian(filter_radius,sS);

  cudaEventRecord(start, 0); // start timer
  //Aufgabe 9: Launch cuda_bilateral_filter() (2 pts)
  cuda_bilateral_filter<<<bilateral_grid, bilateral_block>>>(dev[0], dev[1], input.cols, input.rows, filter_radius, sI, sS); //(BYTE* input, BYTE* output,...)
  cudaEventRecord(stop, 0); // stop timer
  cudaEventSynchronize(stop);

  // Calculate and print kernel run time
  cudaEventElapsedTime(&time, start, stop);
  cout << "GPU Bilateral Filter time: " << time << " (ms)\n";
  cout << "Launched blocks of size " << bilateral_block.x * bilateral_block.y << endl;

  // Copy output from device to host
  //Aufgabe 10: transfer image from device to the main memory for saving onto the disk (1 pt)
  cudaMemcpy(output.pixels, dev[1], sizeof(BYTE)*image_size, cudaMemcpyDeviceToHost);


  // ************** Finalization, cleaning up ************

  // Free GPU variables
  //Aufgabe 11: Free device allocated memory (2 pts)
  cudaFree(dev[0]);
  cudaFree(dev[1]);
  cudaFree(inputImage);
}


