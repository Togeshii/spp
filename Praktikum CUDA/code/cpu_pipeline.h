//
//  cpu_pipeline.h
//
//  Created by Arya Mazaheri on 01/12/2018.
//
//  modified by Yannic Fischler on 01/29/2020
//

#ifndef CPU_PIPELINE_H
#define CPU_PIPELINE_H

#include "ppm.h"

void cpu_pipeline(const Image & input, Image & output, int filter_radius, double sI, double sS);

#endif


