#!/bin/bash
#SBATCH -J Cuda-Lab
#SBATCH -e log.err.%j
#SBATCH -o log.out.%j
#SBATCH -n 1                    # Prozesse
#SBATCH -c 1                    # Kerne pro Prozess
#SBATCH --mem-per-cpu=1600      # Hauptspeicher in MByte pro Rechenkern
#SBATCH -t 00:05:00             # in Stunden und Minuten, oder '#SBATCH -t 10' - nur Minuten
#SBATCH --exclusive
#SBATCH -C "nvd"                # nur Knoten mit nvidia Beschleunigerkarten
#SBATCH --account=kurs00035
#SBATCH --partition=kurs00035
#SBATCH --reservation=kurs00035

# -------------------------------
module purge

module load gcc cuda

./bilateral test_input.ppm 5
