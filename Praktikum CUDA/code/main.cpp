#include "cpu_pipeline.h"
#include "gpu_pipeline.h"

#include "ppm.h"

#include <iostream>

using namespace std;

void compare(const Image & image1, const Image & image2, Image **diff, Image **marker = nullptr) {
  if(image1.rows != image2.rows || image1.cols != image2.cols) {
    std::cerr << "images are not comparable due to size" << std::endl;
    return;
  }

  if(image1.chan != image2.chan) {
    std::cerr << "images are not comparable due to channel" << std::endl;
    return;
  }

  if(image1.ppm_type != image2.ppm_type) {
    std::cerr << "images are not comparable due to ppm_type" << std::endl;
    return;
  }

  (*diff) = new Image(image1.cols, image1.rows, image1.chan, image1.ppm_type);
  if (marker != nullptr)
    (*marker) = new Image(image1.cols, image1.rows, image1.chan, image1.ppm_type);
  for(int i = 0; i < image1.cols * image1.rows * image1.chan; ++i) {
    (*diff)->pixels[i] = image1.pixels[i] - image2.pixels[i];

    if (marker != nullptr) {
      if((*diff)->pixels[i] == 0) {
        (*marker)->pixels[i] = 255;
      }
      else {
        (*marker)->pixels[i] = 0;
        //printf("%d\n", (*diff)->pixels[i]);
      }
    }
  }
}

int main(int argc, char *argv[]) {
	//check for arguments
  if(argc != 3) {
    cerr << "ERROR Usage: bilateral <input_image> <filter_radius_(integer)>" << endl;
    exit(0);
  }
  // Reading input args
  char *img_path = argv[1];
  int filter_radius = atoi(argv[2]);

  if(filter_radius < 1 || filter_radius > 31) {
    cerr << "ERROR Usage: 0 < filter_radius < 32" << endl;
    exit(0);
  }

	Image input = readPPM(img_path);
	// Create output Mat
	Image output_serial = Image(input.cols, input.rows, 1, "P5");
	Image output_gpu = Image(input.cols, input.rows, 1, "P5");

	cpu_pipeline(input, output_serial, filter_radius, 75.0, 75.0);
	savePPM(output_serial, "image_serial.ppm");
	cout << endl;
	gpu_pipeline(input, output_gpu, filter_radius, 75.0, 75.0);
	savePPM(output_gpu, "image_gpu.ppm");

  Image *diff;
  Image *marker;
  compare(output_serial, output_gpu, &diff, &marker);
  savePPM(*diff, "image_diff.ppm");
  savePPM(*marker, "image_marker.ppm");

  return 0;
}
