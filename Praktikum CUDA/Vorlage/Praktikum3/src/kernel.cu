//
//  kernel.cu
//
//  Created by Arya Mazaheri on 01/12/2018.
//

#include <iostream>
#include <algorithm>
#include <cmath>
#include "ppm.h"
#include <cuda_runtime.h>
#include <cuda.h>
#include <cuda_occupancy.h>
#include "device_launch_parameters.h"

//use this to process cudaError_t returned from cuda calls like cudaMalloc(), cudaFree(),...
#define cudaSafeCall(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "CUDA_ERROR: %s @ %s:%d\n", cudaGetErrorString(code), file, line);
	}
}

using namespace std;

/*********** Gray Scale Filter  *********/

/**
 * Converts a given 24bpp image into 8bpp grayscale using the GPU.
 */
__global__
void cuda_grayscale(int width, int height, BYTE *image, BYTE *image_out)
{
	//TODO (9 pt): implement grayscale filter kernel
	int thread = 
		threadIdx.x +threadIdx.y * blockDim.x //position of thread in block
		+ blockIdx.x * blockDim.x *blockDim.y; //position of block in grid
	if (thread > width*height)
		return;

	int pixel_out = thread;
	int pixel_in = 3 * thread; //3 times the pixels because of RGB
	image_out[pixel_out] = image[pixel_in] * 0.0722f + image[pixel_in+1] * 0.7152f + image[pixel_in+2] * 0.2126f; 
}


// 1D Gaussian kernel array values of a fixed size (make sure the number > filter size d)
//TODO: Define the cGaussian array on the constant memory (2 pt)
__constant__ float cGaussian[64];

__host__
void cuda_updateGaussian(int r, double sd)
{
	float fGaussian[64];
	for (int i = 0; i < 2 * r + 1; i++)
	{
		float x = i - r;
		fGaussian[i] = expf(-(x*x) / (2 * sd*sd));
	}
	//TODO: Copy computed fGaussian to the cGaussian on device memory (2 pts)
	cudaMemcpyToSymbol(cGaussian, &fGaussian, 64*sizeof(float), 0, cudaMemcpyHostToDevice); // destination on device, source memory address, size in bytes, offset, type of transfer
}

//TODO: implement cuda_gaussian() kernel (3 pts)
__device__
inline double cuda_gaussian(float x, double sigma) {
    return expf(-(powf(x, 2)) / (2 * powf(sigma, 2)));
}

/*********** Bilateral Filter  *********/
// Parallel (GPU) Bilateral filter kernel
__global__ void cuda_bilateral_filter(BYTE* input, BYTE* output,
	int width, int height,
	int r, double sI, double sS)
{
	//TODO: implement bilateral filter kernel (9 pts)
	int thread = 
		threadIdx.x +threadIdx.y * blockDim.x //position of thread in block
		+ blockIdx.x * blockDim.x *blockDim.y; //position of block in grid
	if (thread > width*height)
		return;


	int h = thread / width;
	int w = thread % width;

	double iFiltered = 0;
	double wP = 0;
	// Get the centre pixel value
	unsigned char centrePx = input[thread];
	// Iterate through filter size from centre pixel
	for (int dy = -r; dy <= r; dy++) {
		int neighborY = h+dy;
		if (neighborY < 0)
            neighborY = 0;
        else if (neighborY >= height)
            neighborY = height - 1;
		for (int dx = -r; dx <= r; dx++) {
			int neighborX = w+dx;
			if (neighborX < 0)
	            neighborX = 0;
	        else if (neighborX >= width)
	            neighborX = width - 1;
			// Get the current pixel; value
			unsigned char currPx = input[neighborY*width+neighborX];
			// Weight = 1D Gaussian(x_axis) * 1D Gaussian(y_axis) * Gaussian(Range or Intensity difference)
			double w = (cGaussian[dy + r] * cGaussian[dx + r]) * cuda_gaussian(centrePx - currPx, sI);
			iFiltered += w * currPx;
			wP += w;				
		}
	}
	output[thread] = iFiltered / wP;

	
}


__host__
void gpu_pipeline(const Image & input, Image & output, int r, double sI, double sS)
{
	// Events to calculate gpu run time
	cudaEvent_t start, stop;
	float time;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	// GPU related variables
	BYTE *d_input = NULL;
	BYTE *d_image_out[2] = { 0 }; //temporary output buffers on gpu device
	int image_size = input.cols*input.rows;
	int suggested_blockSize;   // The launch configurator returned block size 
	int suggested_minGridSize; // The minimum grid size needed to achieve the maximum occupancy for a full device launch

	// ******* Grayscale kernel launch *************

	//Creating the block size for grayscaling kernel
#ifdef __linux__
	cudaOccupancyMaxPotentialBlockSize(&suggested_minGridSize, &suggested_blockSize, cuda_grayscale);
#else
	//windows / cuda 10 code
	cudaSafeCall(cudaSetDevice(0));
	cudaDeviceProp prop;
	cudaSafeCall(cudaGetDeviceProperties(&prop, 0));
	cudaOccDeviceProp oprop = prop;
	cudaOccDeviceState devs;
	cudaOccDeviceStateCheck(&devs);
	cudaFuncAttributes att;
	cudaOccDeviceState state;
	cudaSafeCall(cudaFuncGetAttributes(&att, cuda_grayscale));
	cudaOccFuncAttributes oatt = att;
	cudaOccMaxPotentialOccupancyBlockSize(&suggested_minGridSize, &suggested_blockSize, &oprop, &oatt, &state, 0);
#endif

	int block_dim_x, block_dim_y;
	block_dim_x = block_dim_y = (int)sqrt(suggested_blockSize);

	dim3 gray_block(block_dim_x, block_dim_y); // 2 pts

	//TODO: Calculate grid size to cover the whole image - 2 pts
	dim3 gray_grid(image_size / suggested_blockSize + 1, 1);

	// Allocate the intermediate image buffers for each step
	Image img_out(input.cols, input.rows, 1, "P5");
	//size of image in bytes
	size_t image_byte_size = sizeof(BYTE) *image_size;

	for (int i = 0; i < 2; i++)
	{
		//TODO: allocate memory on the device (2 pts)
		cudaSafeCall(cudaMalloc((void**)&d_image_out[i], image_byte_size));
		//TODO: intialize allocated memory on device to zero (2 pts)
		cudaSafeCall(cudaMemset((void*)d_image_out[i], 0, image_byte_size));
	}

	//copy input image to device
	//TODO: Allocate memory on device for input image (2 pts)
	cudaSafeCall(cudaMalloc((void**)&d_input, image_byte_size * 3));
	//TODO: Copy input image into the device memory (2 pts)
	cudaSafeCall(cudaMemcpy(d_input, input.pixels, image_byte_size*3, cudaMemcpyHostToDevice));

	cudaSafeCall(cudaEventRecord(start, 0)); // start timer
	// Convert input image to grayscale
	//TODO: Launch cuda_grayscale() (2 pts)
	cuda_grayscale << <gray_grid, gray_block >> > (input.cols, input.rows, d_input, d_image_out[0]);
	cudaSafeCall(cudaEventRecord(stop, 0)); // stop timer
	cudaSafeCall(cudaEventSynchronize(stop));

	// Calculate and print kernel run time
	cudaSafeCall(cudaEventElapsedTime(&time, start, stop));
	cout << "GPU Grayscaling time: " << time << " (ms)\n";
	cout << "Launched blocks of size " << gray_block.x * gray_block.y << endl;

	//TODO: transfer image from device to the main memory for saving onto the disk (2 pts)
	//cudaSafeCall(cudaMallocHost(&img_out.pixels, image_byte_size));
	cudaSafeCall(cudaMemcpy(img_out.pixels, d_image_out[0], image_byte_size, cudaMemcpyDeviceToHost));
	savePPM(img_out, "image_gpu_gray.ppm");


	// ******* Bilateral filter kernel launch *************

	//Creating the block size for grayscaling kernel
#ifdef __linux__
	cudaOccupancyMaxPotentialBlockSize(&suggested_minGridSize, &suggested_blockSize, cuda_grayscale);
#else
	cudaSafeCall(cudaSetDevice(0));
	cudaSafeCall(cudaGetDeviceProperties(&prop, 0));
	cudaOccDeviceStateCheck(&devs);
	cudaSafeCall(cudaFuncGetAttributes(&att, cuda_grayscale));
	cudaOccMaxPotentialOccupancyBlockSize(&suggested_minGridSize, &suggested_blockSize, &oprop, &oatt, &state, 0);
#endif

	block_dim_x = block_dim_y = (int)sqrt(suggested_blockSize);

	//TODO:
	dim3 bilateral_block(block_dim_x, block_dim_y); // 2 pts

	//TODO: Calculate grid size to cover the whole image - 2pts
    dim3 bilateral_grid(image_size / suggested_blockSize + 1, 1);

	// Create gaussain 1d array
	cuda_updateGaussian(r, sS);

	cudaSafeCall(cudaEventRecord(start, 0)); // start timer
//TODO: Launch cuda_bilateral_filter() (2 pts)
	cuda_bilateral_filter<<<bilateral_grid, bilateral_block>>>(d_image_out[0], d_image_out[1], input.cols, input.rows, r, sI, sS);
	cudaSafeCall(cudaEventRecord(stop, 0)); // stop timer
	cudaSafeCall(cudaEventSynchronize(stop));

	// Calculate and print kernel run time
	cudaSafeCall(cudaEventElapsedTime(&time, start, stop));
	cout << "GPU Bilateral Filter time: " << time << " (ms)\n";
	cout << "Launched blocks of size " << bilateral_block.x * bilateral_block.y << endl;

	// Copy output from device to host
//TODO: transfer image from device to the main memory for saving onto the disk (2 pts)
	cudaSafeCall(cudaMemcpy(output.pixels, d_image_out[1], image_byte_size, cudaMemcpyDeviceToHost));

	// ************** Finalization, cleaning up ************

	// Free GPU variables
//TODO: Free device allocated memory (3 pts)
	//cudaSafeCall(cudaFreeHost(img_out.pixels));
	cudaSafeCall(cudaFree(d_input));
	cudaSafeCall(cudaFree(d_image_out[0]));
	cudaSafeCall(cudaFree(d_image_out[1]));
}
