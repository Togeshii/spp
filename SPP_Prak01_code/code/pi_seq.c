#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <assert.h>

#include <omp.h>

#define STATESIZE 64

// returns random float in range -1 to 1
double rand_dbl(struct random_data *random_buf) {
  int rand_int;
  random_r(random_buf, &rand_int);
  return (double)rand_int / RAND_MAX * 2.0 - 1.0;
}

int main(int argc, char *argv[]) {
  // reading the number of points to use from the argument
  unsigned long num_points = 100000;
  if (argc > 1) {
    num_points = atol(argv[1]);
  }

  // be aware that each thread should have its own random state
  struct random_data *random_buf = calloc(1, sizeof(struct random_data));
  char *state_buf = calloc(STATESIZE, sizeof(char));
  // to be clear: random_data and state_buf are both part of the random state
  assert(random_buf != NULL && state_buf != NULL);
  // threads should use a different random seed
  unsigned int seed = time(NULL);
  initstate_r(seed, state_buf, STATESIZE, random_buf);

  // you may get a random point like this:
  //double x = rand_dbl(random_buf);
  //double y = rand_dbl(random_buf);
  double x,y;
  int inCircle = 0;
  
  for(int i=1; i<=num_points; i++){
	  x = rand_dbl(random_buf);
	  y = rand_dbl(random_buf);
	  if(pow(x,2)+pow(y,2) < 1) // im Kreis
		inCircle++;
  }

  double pi = 4 * (double)inCircle / num_points ;

  printf("Pi is approx. %f\n", pi);

  free(random_buf);
  free(state_buf);

  return 0;
}
