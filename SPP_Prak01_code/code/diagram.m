ergebnisse = load('times.txt');
if (size(ergebnisse,2) > 2)
  mean_times = mean(ergebnisse(:,2:end)')';
else
  mean_times = ergebnisse(:,2);
endif

hf = figure();

% entweder
%h = plot(1:6, mean_times)
%set(gca,'XTickLabel',ergebnisse(:,1)', "linewidth", 3, "fontsize", 20);
% oder
h = plot(ergebnisse(:,1), mean_times)
set(gca,'XTick',ergebnisse(:,1)', "linewidth", 3, "fontsize", 20);

xlabel("Threads");
ylabel("Time in ?");
title("Heated-Plate-Parallel with diffrent number of threads");
set(h, "linewidth", 3);

%print (hf, "plot.pdf");
print (hf, "plot.pdf", "-dpdflatexstandalone");
system ("pdflatex plot");
%open plot.pdf